<?php

namespace Database\Seeders;

use App\Models\NewsApiSource;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ApiNewsSourceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $newsApiSourceObject = new NewsApiSource();
        $newsApiSourceObject->updateSourceList();
    }
}
