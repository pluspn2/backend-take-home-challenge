<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('v1')->group(function (){
   Route::resource('news',\App\Http\Controllers\Api\V1\NewsController::class)->only(['index','show']);
   Route::resource('categories',\App\Http\Controllers\Api\V1\CategoryController::class)->only('index');
});
