<?php

namespace App\Console;

use App\Models\NewsApi;
use App\Models\TheGuardian;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule): void
    {
        $schedule->call(function (){
            $newsApi = new NewsApi();
            $newsApi->updateNews();

            $theGuardian = new TheGuardian();
            $theGuardian->updateNews();
        })->daily();
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
