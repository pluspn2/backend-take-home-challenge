<?php
namespace App\Services;
use Spatie\Sluggable\SlugOptions;

trait generateSlugFromTitle{
    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }
}
