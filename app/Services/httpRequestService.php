<?php
namespace App\Services;
use Illuminate\Support\Facades\Http;

trait httpRequestService{

    public function makeRequest($url,$method)
    {
        return Http::$method($url);
    }
}
