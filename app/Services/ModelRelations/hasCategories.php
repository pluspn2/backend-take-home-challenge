<?php
namespace App\Services\ModelRelations;
use App\Models\Category;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

trait hasCategories{

    public function categories():MorphToMany
    {
        return $this->morphToMany(Category::class, 'model','model_categories');
    }
}
