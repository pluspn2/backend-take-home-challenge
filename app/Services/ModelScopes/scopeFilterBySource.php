<?php
namespace App\Services\ModelScopes;
use Illuminate\Database\Eloquent\Builder;

trait scopeFilterBySource{
    public function scopeFilterBySource(Builder $query, $source = null)
    {
        if($source)
            return $query->where('author','like','%'.$source.'%');
        return $query;
    }
}
