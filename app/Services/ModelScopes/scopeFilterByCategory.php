<?php
namespace App\Services\ModelScopes;
use Illuminate\Database\Eloquent\Builder;

trait scopeFilterByCategory{
    public function scopeFilterByCategory(Builder $query, $category = null)
    {
        if($category)
            return $query->whereHas('categories',function (Builder $q) use ($category) {
               return $q->where('name','like','%'.$category.'%');
            });
    }
}
