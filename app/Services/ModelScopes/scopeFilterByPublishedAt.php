<?php
namespace App\Services\ModelScopes;
use Illuminate\Database\Eloquent\Builder;

trait scopeFilterByPublishedAt{
    //Greater than equal || Minimum
    public function scopeFilterByPublishedAtGTE(Builder $query, $date = null)
    {
        if($date)
            return $query->whereDate('published_at','>=',$date);
        return $query;
    }

    //Lower than equal || Maximum
    public function scopeFilterByPublishedAtLTE(Builder $query, $date = null)
    {
        if($date)
            return $query->whereDate('published_at','<=',$date);
        return $query;
    }
}
