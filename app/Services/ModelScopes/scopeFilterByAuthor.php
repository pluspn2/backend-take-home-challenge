<?php
namespace App\Services\ModelScopes;
use Illuminate\Database\Eloquent\Builder;

trait scopeFilterByAuthor{
    public function scopeFilterByAuthor(Builder $query, $author = null)
    {
        if($author)
            return $query->where('author','like','%'.$author.'%');
        return $query;
    }
}
