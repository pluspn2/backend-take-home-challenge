<?php
namespace App\Services;

trait getNewsService{
    use httpRequestService;

    public function getNews($query = null)
    {
        $url = $this->base_url."?".$this->api_key_key_in_url."=".$this->api_key.$query;
        return $this->makeRequest($url,"get");
    }
}
