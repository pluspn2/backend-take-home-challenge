<?php

namespace App\Observers;

use App\Models\Article;
use App\Models\NewsApi;
use App\Models\NewsApiSource;

class ArticleObserver
{
    /**
     * Handle the Article "created" event.
     */
    public function created(Article $article): void
    {
        if($article->source_api_model == NewsApi::class){
            $newsApiSource = NewsApiSource::findByName($article->source);
            foreach ($newsApiSource->categories as $category){
                $article->addCategory($category->name);
            }
        }
    }

    /**
     * Handle the Article "updated" event.
     */
    public function updated(Article $article): void
    {
        //
    }

    /**
     * Handle the Article "deleted" event.
     */
    public function deleted(Article $article): void
    {
        //
    }

    /**
     * Handle the Article "restored" event.
     */
    public function restored(Article $article): void
    {
        //
    }

    /**
     * Handle the Article "force deleted" event.
     */
    public function forceDeleted(Article $article): void
    {
        //
    }
}
