<?php

namespace App\Http\Requests\Api\V1\News;

use Illuminate\Foundation\Http\FormRequest;

class IndexNewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'date_gte' => ['nullable','date'],
            'date_lte' => ['nullable','date'],
            'category' => ['nullable','string'],
            'author' => ['nullable','string'],
            'source' => ['nullable','string'],
        ];
    }
}
