<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\News\IndexNewsRequest;
use App\Http\Resources\Api\V1\News\NewsItemResource;
use App\Models\Article;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(IndexNewsRequest $request)
    {
        $news = Article::query()
            ->filterByPublishedAtGTE($request->date_gte)
            ->filterByPublishedAtLTE($request->date_lte)
            ->filterByCategory($request->category)
            ->filterByAuthor($request->author)
            ->filterBySource($request->source)
            ->latest()
            ->paginate();
        return NewsItemResource::collection($news);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Article $news)
    {
        return NewsItemResource::make($news);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
