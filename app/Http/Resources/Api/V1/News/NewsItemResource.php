<?php

namespace App\Http\Resources\Api\V1\News;

use App\Http\Resources\Api\V1\Category\CategoryItemResouce;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class NewsItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'title' => $this->title,
            'slug' => $this->slug,
            'source' => $this->source,
            'author' => $this->author,
            'url' => $this->url,
            'published_at' => $this->published_at,
            'categories' => CategoryItemResouce::collection($this->categories)
        ];
    }
}
