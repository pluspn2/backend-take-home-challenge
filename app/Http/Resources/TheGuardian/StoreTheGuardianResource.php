<?php

namespace App\Http\Resources\TheGuardian;

use App\Models\TheGuardian;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class StoreTheGuardianResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'title' => $this['webTitle'],
            'source' => 'The Guardian',
            'source_api_model' => TheGuardian::class,
            'author' => 'The Guardian',
            'url' => $this['webUrl'],
            'published_at' => Carbon::parse($this['webPublicationDate']),
        ];
    }
}
