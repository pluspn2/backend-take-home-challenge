<?php

namespace App\Http\Resources\NewsApi;

use App\Models\NewsApi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class StoreNewsApiResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'title' => $this['title'],
            'source' => $this['source']['name'],
            'author' => $this['author'],
            'url' => $this['url'],
            'published_at' => Carbon::parse($this['publishedAt']),
            'source_api_model' => NewsApi::class,
        ];
    }
}
