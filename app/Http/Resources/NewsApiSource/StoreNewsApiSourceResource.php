<?php

namespace App\Http\Resources\NewsApiSource;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class StoreNewsApiSourceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'source_id' => $this['id'],
            'name' => $this['name'],
            'description' => $this['description'],
            'url' => $this['url'],
            'language' => $this['language'],
            'country' => $this['country']
        ];
    }
}
