<?php

namespace App\Models;

use App\Services\generateSlugFromTitle;
use App\Services\ModelRelations\hasCategories;
use App\Services\ModelScopes\scopeFilterByAuthor;
use App\Services\ModelScopes\scopeFilterByCategory;
use App\Services\ModelScopes\scopeFilterByPublishedAt;
use App\Services\ModelScopes\scopeFilterBySource;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;

class Article extends Model
{
    use HasFactory;
    use HasSlug;

    //services
    use generateSlugFromTitle;

    //relations
    use hasCategories;

    //scopes
    use scopeFilterByPublishedAt;
    use scopeFilterByAuthor;
    use scopeFilterByCategory;
    use scopeFilterBySource;

    protected $fillable = [
        'title',
        'slug',
        'source',
        'source_api_model',
        'author',
        'url',
        'published_at',
    ];

    public function addCategory(string $categoryName)
    {
        $category = Category::query()->firstOrCreate(['name' => $categoryName],['name' => $categoryName]);
        $this->categories()->attach($category->id);
    }

    public function getRouteKeyName()
    {
        return "slug";
    }
}
