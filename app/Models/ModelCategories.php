<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelCategories extends Model
{
    use HasFactory;
    protected $fillable = [
        'category_id',
        'model_name',
        'model_id'
    ];
}
