<?php

namespace App\Models;

use App\Http\Resources\NewsApi\StoreNewsApiResource;
use App\Interfaces\NewsInterface;
use App\Services\getNewsService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\JsonResource;

class NewsApi extends Model implements NewsInterface
{
    use HasFactory;
    use getNewsService;
    protected $api_key;
    protected $base_url = "https://newsapi.org/v2/everything";
    protected $api_key_key_in_url = "apiKey";
    protected $sample = "https://newsapi.org/v2/everything?q=tesla&from=2023-11-09&sortBy=publishedAt&apiKey=49a55d84b5314e569f66a57a17a20e7d";

    public function __construct(array $attributes = [])
    {
        $this->api_key = env('NEWSAPI');
        parent::__construct($attributes);
    }

    public function getTodayNewsFromSource($source)
    {
        $today = Carbon::now()->addDays(-1)->format('Y-m-d');
        return $this->getNews("&from=".$today."&sources=".$source);
    }

    public function checkTotalResultsAvailableInNewsResponseAndTotalResultsAtLeastOne($newsResponse):bool
    {
        return (isset($newsResponse['totalResults']) && $newsResponse['totalResults'] > 0);
    }

    public function checkArticlesAvailableInNewsResponseAndArticleTypesIsArray($newsResponse):bool
    {
        return (isset($newsResponse['articles']) && is_array($newsResponse['articles']));
    }

    public function createArticleFromItem($item):Article
    {
        $articleFormatForArticleModel = StoreNewsApiResource::make($item)->jsonSerialize();
        return Article::create($articleFormatForArticleModel);
    }

    public function getSourcesAttribute()
    {
        return NewsApiSource::query()->pluck('source_id')->toArray();
    }

    public function updateNews()
    {
        $sources = $this->sources;
        foreach ($sources as $source){
            $newsResponse = $this->getTodayNewsFromSource($source);
            if($this->checkTotalResultsAvailableInNewsResponseAndTotalResultsAtLeastOne($newsResponse)){
                if($this->checkArticlesAvailableInNewsResponseAndArticleTypesIsArray($newsResponse)){
                    foreach ($newsResponse['articles'] as $articleItem){
                        $this->createArticleFromItem($articleItem);
                    }
                }
            }
        }

    }
}
