<?php

namespace App\Models;

use App\Http\Resources\TheGuardian\StoreTheGuardianResource;
use App\Interfaces\NewsInterface;
use App\Services\getNewsService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TheGuardian extends Model implements NewsInterface
{
    use HasFactory;
    use getNewsService;
    protected $api_key;
    protected $base_url = "https://content.guardianapis.com/search";
    protected $api_key_key_in_url = "api-key";
    protected $sample = "https://content.guardianapis.com/search?api-key=9e742352-5a58-463e-826c-9be7612f4e73";

    public function __construct(array $attributes = [])
    {
        $this->api_key = env('THEGUARDIANAPI');
        parent::__construct($attributes);
    }

    public function createArticleFromItem($item):Article
    {
        $articleFormatForArticleModel = StoreTheGuardianResource::make($item)->jsonSerialize();
        return Article::create($articleFormatForArticleModel);
    }

    public function updateNews()
    {
        $newsResponse = $this->getNews();
        $pages = $newsResponse['response']['pages'];
        $results = $newsResponse['response']['results'];
        $current_page = 1;
        while($current_page <= $pages){
            foreach ($results as $resultItem){
                $_article = $this->createArticleFromItem($resultItem);
                $_article->addCategory($resultItem['pillarName']);
            }
            $current_page = $current_page + 1;
            $newsResponse = $this->getNews("&page=".$current_page);
        }
    }
}
