<?php

namespace App\Models;

use App\Http\Resources\NewsApiSource\StoreNewsApiSourceResource;
use App\Services\httpRequestService;
use App\Services\ModelRelations\hasCategories;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewsApiSource extends Model
{
    use HasFactory;
    use httpRequestService;
    use hasCategories;

    protected $api_key;
    protected $base_url = 'https://newsapi.org/v2/top-headlines/sources?apiKey=';
    protected $fillable = [
        'source_id',
        'name',
        'description',
        'url',
        'language',
        'country'
    ];

    public function __construct(array $attributes = [])
    {
        $this->api_key = env('NEWSAPI');
        parent::__construct($attributes);
    }

    public static function findByName($name)
    {
        return NewsApiSource::query()->where('name','=',$name)->first();
    }

    public function getSourceFromApi()
    {
        return $this->makeRequest($this->base_url.$this->api_key,'get');
    }

    public function firstOrCreateNewsSource($source)
    {
        return NewsApiSource::query()->firstOrCreate(
            ['source_id' => $source['id']],
            StoreNewsApiSourceResource::make($source)->jsonSerialize()
        );
    }

    public function addCategory(string $categoryName)
    {
        $category = Category::query()->firstOrCreate(['name' => $categoryName],['name' => $categoryName]);
        $this->categories()->attach($category->id);
    }

    public function updateSourceList()
    {
        $sourcesResponse = $this->getSourceFromApi();
        if(isset($sourcesResponse['sources']) && count($sourcesResponse['sources']) > 0 ){
            foreach ($sourcesResponse['sources'] as $source){
                $newsApiSource = $this->firstOrCreateNewsSource($source);
                $newsApiSource->addCategory($source['category']);
            }
        }
    }
}
