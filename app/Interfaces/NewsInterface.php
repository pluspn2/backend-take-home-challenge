<?php
namespace App\Interfaces;
interface NewsInterface
{
    public function getNews();
    public function updateNews();
    public function createArticleFromItem($item);
}
