## Backend Take-Home challenge

This is case study task for [innoscripta.com](https://innoscripta.com)  company
based on laravel 10

### Api souces:
-[NewsApi](https://newsapi.org) <br>
-[The Guardian](https://www.theguardian.com/)

### What I've done
- [x] Article model for news <br>
- [x] NewsApi TheGuardian model for updating news from api <br>
- [x] Daily cronjob to update news <br>
- [x] Create NewApiSources model to support multiple sources <br>
- [x] Categories relation <br>
- [x] News and Categories endpoints <br>
- [x] Scopes for Article model <br>
- [x] Article Observer for updating categories that fetched from NewsApi <br>
- [x] Create NewsInterface <br>


### [Postman Document](https://documenter.getpostman.com/view/13815129/2s9YXk31SB)


## ToDo:
- [ ] prevent store duplicate news
